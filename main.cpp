#include <string>
#include <iostream>
#include "linkedlist.h"
#include "mp3.h"
#include "database.h"
#include <cassert>
#include <fstream>
#include "genres.h"
using namespace std;

//void createLL();
LinkedList<mp3*> *DJBList = new LinkedList<mp3*>;
string genres[148];
string filename[30];
int count = 12;
int arrcnt = 0;
	

int main()
{
	initialize_genres();
	filename[0] = "01_Just_Imagine_vbr.mp3";
	filename[1] = "02_Pulse_of_the_Earth_vbr.mp3";
	filename[2] = "03_Balloon_Girl_vbr.mp3";
	filename[3] = "04_Bumble_vbr.mp3";
	filename[4] = "05_Hill_vbr.mp3";
	filename[5] = "06_Simone_vbr.mp3";
	filename[6] = "07_Voyeur_vbr.mp3";
	filename[7] = "08_The_Standing_Ones_vbr.mp3";
	filename[8] = "09_Sunday_Smiled_vbr.mp3";
	filename[9] = "10_Wandering_vbr.mp3";
	filename[10] = "01_999999_vbr.mp3";
	
	ifstream f1(filename[0].c_str(), ios::binary);
	
	mp3 *firstsong = new mp3(f1);
	
	DJBList->push_back(firstsong);
	
	assert(DJBList->length() == 1);
	
	ifstream f2(filename[1].c_str(), ios::binary);
	
	mp3 *secondsong = new mp3(f2);
	
	DJBList->push_back(secondsong);
	
	assert(DJBList->length() == 2);
	
	ifstream f3(filename[2].c_str(), ios::binary);
	
	mp3 *thirdsong = new mp3(f3);
	
	DJBList->push_back(thirdsong);
	
	assert(DJBList->length() == 3);
	
	ifstream f4(filename[3].c_str(), ios::binary);
	
	mp3 *fourthsong = new mp3(f4);
	
	DJBList->push_back(fourthsong);
	
	assert(DJBList->length() == 4);
	
	ifstream f5(filename[4].c_str(), ios::binary);
	
	mp3 *fifthsong = new mp3(f5);
	
	DJBList->push_back(fifthsong);
	
	assert(DJBList->length() == 5);
	
	ifstream f6(filename[5].c_str(), ios::binary);
	
	mp3 *sixthsong = new mp3(f6);
	
	DJBList->push_back(sixthsong);
	
	assert(DJBList->length() == 6);
	
	ifstream f7(filename[6].c_str(), ios::binary);
	
	mp3 *seventhsong = new mp3(f7);
	
	DJBList->push_back(seventhsong);
	
	assert(DJBList->length() == 7);
	
	ifstream f8(filename[7].c_str(), ios::binary);
	
	mp3 *eighthsong = new mp3(f8);
	
	DJBList->push_back(eighthsong);
	
	assert(DJBList->length() == 8);
	
	ifstream f9(filename[8].c_str(), ios::binary);
	
	mp3 *ninethsong = new mp3(f9);
	
	DJBList->push_back(ninethsong);
	
	assert(DJBList->length() == 9);
	
	ifstream f10(filename[9].c_str(), ios::binary);
	
	mp3 *tenthsong = new mp3(f10);
	
	DJBList->push_back(tenthsong);
	
	assert(DJBList->length() == 10);

	ifstream f11(filename[10].c_str(), ios::binary);
	
	mp3 *eleventhsong = new mp3(f11);
	
	DJBList->push_back(eleventhsong);
	
	assert(DJBList->length() == 11);
	
	mp3 **adds = new mp3*[20];
	
	ifstream f[10];
	
	int ad = 0;
	
	//More can be added, Just chose to keep it small
	//Instead I made a add part to the project (P.S, can break)
	
	string query;
	//createLL();
	cout<<"Welcome to DJ Belatoran's MP3 Listings!!!"<<endl;
	cout<< endl;
	cout<<"Type 'list' or enter a query: [artist/title/album/year/all/add] [query/file name and extension(mp3)]"<<endl;
	cout<<endl;
	cout<<"REMINDER: MAKE SURE ANY FILES ADDED TO THE SYSTEM ARE IN THE PROPER DIRECTORY"<<endl;
	cout<<endl;
	getline(cin, query);
	cout<<endl;
	ad = query.find("add");
	if(query == "quit"){
		delete firstsong,secondsong,thirdsong,fourthsong,fifthsong,sixthsong,seventhsong,eighthsong,ninethsong,tenthsong,eleventhsong;
		delete DJBList;
			return 0;
	}
	else if(ad > -1){
		query = query.substr(query.find(' ') + 1);
		f[arrcnt].open(query.c_str(),ios::binary);
		adds[arrcnt] = new mp3(f[arrcnt]);
		DJBList->push_back(adds[arrcnt]);
		assert(DJBList->length() == count);
		arrcnt++;
		count++;
		query = "list";
		database(DJBList,query);
		
		
	}
	else{
	database(DJBList,query);
	}
	while(true){
		cout<< "Is there anything else DJ Belatoran can do for you?"<<endl;
		cout<<endl;
		getline(cin,query);
		cout<<endl;
		ad = query.find("add");
		if(query == "quit"){
			delete firstsong,secondsong,thirdsong,fourthsong,fifthsong,sixthsong,seventhsong,eighthsong,ninethsong,tenthsong;
			delete DJBList;
			
			return 0;
		}
		else if(ad > -1){
		query = query.substr(query.find(' ') + 1);
		f[arrcnt].open(query.c_str(),ios::binary);
		adds[arrcnt] = new mp3(f[arrcnt]);
		DJBList->push_back(adds[arrcnt]);
		assert(DJBList->length() == count);
		arrcnt++;
		count++;
		query = "list";
		database(DJBList,query);
		
		
		}
		else{
		database(DJBList,query);
		}
		
	}
	
	
	
}
