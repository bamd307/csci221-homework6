#include <string>
#include <iostream>
#include "mp3.h"
#include "database.h"
#include <fstream>
#include <cassert>
#include <algorithm>
extern string genres[148];

int filelength;
char ttl[31];
char art[31];
char alb[31];
char yr[5];
char cmmnt[31];
char gnr[2];
		
string Title;
string Artist;
string Album;
string Year;
string Comment;
string Genre;



void mp3::setTitle(string title){
	Title = title;
	transform(Title.begin(), Title.end(), Title.begin(),::tolower);
}
void mp3::setArtist(string artist){
	Artist = artist;
	transform(Artist.begin(), Artist.end(), Artist.begin(),::tolower);
}
void mp3::setAlbum(string album){
	Album = album;
	transform(Album.begin(), Album.end(), Album.begin(),::tolower);
}
void mp3::setYear(string year){
	Year = year;
	transform(Year.begin(), Year.end(), Year.begin(),::tolower);
}
void mp3::setComment(string comment){
	Comment = comment;
	transform(Comment.begin(), Comment.end(), Comment.begin(),::tolower);
}
void mp3::setGenre(string genre){
	int value = atoi(genre.c_str());
	if(value > 0 && value < 148){	
	Genre = genres[value];
	}
	else{
		Genre = "unknown";
	}

	transform(Genre.begin(), Genre.end(), Genre.begin(),::tolower);
}
void mp3::printAll(){
	cout<<Title<<" - "<<Artist<<" ("<<Album<<" , "<<Year<<") ["<<Comment<<"] "<<Genre<<endl;
	cout<<endl;
}
void mp3::setAll(ifstream &file){
	
	file.seekg(0,file.end);										//is.seekg (0, is.end);
  	filelength = file.tellg();									//int length = is.tellg();
    file.seekg(filelength - 125);								//is.seekg (0, is.beg);
	file.read(ttl, 30);
	ttl[30] = 0;
	setTitle(string(ttl));
	file.read(art,30);
	art[30] = 0;
	setArtist(string(art));
	file.read(alb, 30);
	alb[30] = 0;
	setAlbum(string(alb));
	file.read(yr,4);
	yr[4] = 0;
	setYear(string(yr));
	file.read(cmmnt, 30);
	cmmnt[30] = 0;
	setComment(string(cmmnt));
	file.read(gnr, 1);
	gnr[1] = 0;
	setGenre(string(gnr));
}

	
