#include <string>
#include <iostream>
#include "linkedlist.h"
#include "mp3.h"
#include <cassert>
#include <fstream>
#include "database.h"

using namespace std;

database::database(LinkedList<mp3*> *list,string query){
	int i = query.find(' ');
	if(query.substr(0,i) == "title"){
		query = query.substr(i+1);
		searchTitle(list,query);
	}
	else if(query.substr(0,i) == "artist"){
		query = query.substr(i+1);
		searchArtist(list,query);
	}
	else if(query.substr(0,i) == "album"){
		query = query.substr(i+1);
		searchAlbum(list,query);
	}
	else if(query.substr(0,i) == "year"){
		query = query.substr(i+1);
		searchYear(list,query);
	}	
	else if(query.substr(0,i) == "all"){
		query = query.substr(i+1);
		searchAll(list,query);
	}
	else if(query.substr(0,i) == "list"){
		showAll(list);
	}

	else{
		cout<<"UH-OH: NOTHING FOUND!"<<endl;
		cout<<endl;
	}
}

	void database::searchTitle(LinkedList<mp3*> *list,string query){
		list->reset();
		int bleh = 0;
		for(int i = 0; i < list->length();i++){
			bleh = list->get_val()->getTitle().find(query);
			if(bleh > -1){
				list->get_val()->printAll();
				list->next();
			}
			else{
				list->next();
			}
			
			
		}
	}
	void database::searchArtist(LinkedList<mp3*> *list,string query){
		list->reset();
		int bleh = 0;
		for(int i = 0; i < list->length();i++){
			bleh = list->get_val()->getArtist().find(query);
			if(bleh > -1){
				list->get_val()->printAll();
				list->next();
			}
			else{
				list->next();
			}
			
			
		}
		
	}
	void database::searchAlbum(LinkedList<mp3*> *list,string query){
		list->reset();
		int bleh = 0;
		for(int i = 0; i < list->length();i++){
			bleh = list->get_val()->getAlbum().find(query);
			if(bleh > -1){
				list->get_val()->printAll();
				list->next();
			}
			else{
				list->next();
			}
			
			
		}
		
	}
	void database::searchYear(LinkedList<mp3*> *list,string query){
		list->reset();
		int bleh = 0;
		for(int i = 0; i < list->length();i++){
			bleh = list->get_val()->getYear().find(query);
			if(bleh > -1){
				list->get_val()->printAll();
				list->next();
			}
			else{
				list->next();
			}
			
			
		}
		
	}
	void database::showAll(LinkedList<mp3*> *list){
		list->reset();
		for(int i = 0; i < list->length();i++){
			list->get_val()->printAll();
			list->next();
		}
	}
	
	void database::searchAll(LinkedList<mp3*> *list,string query){
		list->reset();
		int title,artist,album,year;
		for(int i = 0; i < list->length();i++){
			title = list->get_val()->getTitle().find(query);
			artist = list->get_val()->getArtist().find(query);
			album = list->get_val()->getAlbum().find(query);
			year = list->get_val()->getYear().find(query);
			
			if((title > -1) || (artist > -1) || (album > -1) || (year > -1)){
				list->get_val()->printAll();
				list->next();
			}
			else{
				list->next();
			}
			
			
		}
		
	}
	
	
	
	
	
