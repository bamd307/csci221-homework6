#ifndef NODE_H
#define NODE_H
#include <cstdlib>

template <typename T>
class Node
{
public:
    Node()
    {
	next = NULL;
    }

    T get_val()
    {
	return val;
    }

    void set_val(T val)
    {
	this->val = val;
    }

    Node *get_next()
    {
	return next;
    }

    void set_next(Node *next)
    {
	this->next = next;
    }
private:
    T val; //
    Node *next;
};

#endif
