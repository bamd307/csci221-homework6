#ifndef LINKED_LIST_H
#define LINKED_LIST_H

#include "node.h"
#include <iostream>
#include <cstdlib>
#include <assert.h>

template <typename T>
class LinkedList
{
public:
    LinkedList()
    {
        head = NULL;
        current = NULL;
        count = 0;
    }

    void next()
    {
        current = current->get_next();
    }

    void reset()
    {
        current = head;
    }

    bool end()
    {
        return (current == NULL);
    }

    T get_val()
    {
        assert(current != NULL);
        return current->get_val();
    }

    void set_val(T val); //
    void insert_front(T val)
    {
        Node<T> *n = new Node<T>;
        n->set_val(val);
        n->set_next(head);
        head = n;
        count++;
    }

    void push_back(T val)
    {
        if(count == 0)
        {
            insert_front(val);
        }
        else
        {
            Node<T> *n = new Node<T>;
            n->set_val(val);
            Node<T> *nlast = node_at(count - 1);
            nlast->set_next(n);
            count++;
        }
    }

    void remove_at(int index)
    {
        // fails if index is bogus; also fails if count == 0, so we know
        // the list is not empty
        //assert(index >= 0 && index < count);
        if(index == 0)
        {
            Node<T> *ndelete = head;
            head = head->get_next(); // might be NULL; that's ok
            delete ndelete;
            count--;
        }
        else
        {
            Node<T> *prev = node_at(index - 1);
            Node<T> *ndelete = prev->get_next();
            prev->set_next(ndelete->get_next());
            delete ndelete;
            count--;
        }
    }

    T val_at(int index)
    {
        assert(index >= 0 && index < count);
        return (node_at(index)->get_val());
    }

    void remove_duplicates()
    {
        // temporary linked list containing vals already seen
        LinkedList<T> *seen = new LinkedList<T>;
        int i = 0;
        while(i < length()) // be sure length() is recomputed every time
        {
            T val = val_at(i);
            if(seen->contains(val))
            {
                remove_at(i);
                // don't increase i
            }
            else
            {
                seen->insert_front(val);
                i++;
            }
        }
        delete seen;
    }

    bool contains(T val)
    {
        Node<T> *pnode = head;
        while(pnode != NULL)
        {
            if(pnode->get_val() == val)
            {
                return true;
            }
            pnode = pnode->get_next();
        }
        return false;
    }

    int length()
    {
        return count;
    }

    LinkedList<T> *copy()
    {
        LinkedList<T> *copy_list = new LinkedList<T>;
        Node<T> *pnode = head;
        while(pnode != NULL)
        {
            copy_list->push_back(pnode->get_val());
            pnode = pnode->get_next();
        }
        return copy_list;
    }

    void print()
    {
        Node<T> *pnode = head;
        while(pnode != NULL)
        {
            std::cout << pnode->get_val() << std::endl;
            pnode = pnode->get_next();
        }
    }

    ~LinkedList()
    {
        Node<T> *pnode = head;
        while(pnode != NULL)
        {	
            Node<T> *ndelete = pnode;
            pnode = pnode->get_next();
            delete ndelete;
        }
    }
    
private:
    Node<T> *node_at(int index)
    {
        assert(index >= 0 && index < count);
        Node<T> *n = head;
        for(int i = 0; i < index; i++)
        {
            n = n->get_next();
        }
        return n;
    }

    Node<T> *head;
    Node<T> *current;
    int count;
};

#endif
