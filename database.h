#include <string>
#include <iostream>
#include "linkedlist.h"
#include "mp3.h"
#include <cassert>
#include <fstream>

class database{
	public:
		
		database(LinkedList<mp3*> *list, string query);
		
		void nextSearch(string query);
		
	private:
		
		LinkedList<mp3*> list;
		
		void searchTitle(LinkedList<mp3*> *list,string query);
		void searchArtist(LinkedList<mp3*> *list,string query);
		void searchAlbum(LinkedList<mp3*> *list,string query);
		void searchYear(LinkedList<mp3*> *list,string query);
		void showAll(LinkedList<mp3*> *list);
		void searchAll(LinkedList<mp3*> *list,string query);
		
			
		
};
