CXX = g++
CXXFLAGS = -Wall -ansi -pedantic -ggdb3 -lboost_filesystem

all: main

main: main.o mp3.o database.o linkedlist.h node.h
	$(CXX) $(CXXFLAGS) -o main main.o mp3.o database.o

main.o: main.cpp database.h genres.h
	$(CXX) $(CXXFLAGS) -c main.cpp

database.o: database.cpp database.h mp3.h linkedlist.h
	$(CXX) $(CXXFLAGS) -c database.cpp

mp3.o: mp3.cpp mp3.h genres.h
	$(CXX) $(CXXFLAGS) -c mp3.cpp

.PHONY: clean
clean:
	rm -rf *.o main


