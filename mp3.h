#ifndef MP3_H
#define MP3_H
#include<string>
#include <fstream>
using namespace std;

class mp3
{
	public:
		mp3(){
			Title = "";
			Artist = "";
			Album = "";
			Year = "";
			Comment = "";
			Genre = "";
		}
		
		mp3(ifstream &file){
			setAll(file);
		}		
		
		void printAll();

		
		
		string getTitle()
		{
			return Title;
		}
		
		string getArtist()
		{
			return Artist;
		}
		
		string getAlbum()
		{
			return Album;
		}
		
		string getYear()
		{
			return Year;
		}
		
		string getComment()
		{
			return Comment;
		}
		
		string getGenre()
		{
			return Genre;
		}
		
		
	private:
	
	string Title;
	string Artist;
	string Album;
	string Year;
	string Comment;
	string Genre;
	
	int filelength;
	char ttl[31];
	char art[31];
	char alb[31];
	char yr[5];
	char cmmnt[31];
	char gnr[2];	
	
	void setAll(ifstream &file);	
	void setTitle(string title);
	void setArtist(string artist);
	void setAlbum(string album);
	void setYear(string year);
	void setComment(string comment);
	void setGenre(string genre);		
		
	
};
#endif
